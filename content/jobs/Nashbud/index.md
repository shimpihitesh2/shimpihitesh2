---
date: '4'
title: 'Android Developer Intern'
company: 'Nashbud'
info: 'Android Application Development'
range: 'Aug 2022 - Sept 2022'
---

- Target was to improvise the exisiting application to make it competent to publish on Play Store
- Responsible for stabilizing the current state and adding hands on features before rolling out the first release
