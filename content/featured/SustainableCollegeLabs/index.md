---
date: '3'
title: 'SportsFam'
cover: 'Sportsfam.png'
github: 'https://github.com/SportsFam-01'
tech:
  - FLutter
  - NodeJS
  - Express
  - AWS Lambda
  - DynamoDB
  - API Gateway
  - Github Actions

showInProjects: true
---

Crafted for sports enthusiasts, this mobile application fosters a vibrant community where users can share their sporting activities and achievements, earning rewards along the way. Harnessing the robust capabilities of AWS, the backend infrastructure ensures scalability and high availability, guaranteeing uninterrupted access for users across the globe.
