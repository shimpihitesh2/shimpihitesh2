---
date: '5'
title: 'Smart Support Ticketing System'
cover: 'CollegeSarthi.png'
github: 'https://gitlab.com/shimpihitesh2/college-sarthi-app'
external: 'https://gitlab.com/shimpihitesh2/tyrion'
tech:
  - FLutter
  - Django
  - NginX
  - Gunicorn
  - PostgreSQL
  - EC2
  - Tensorflow
  - React
showInProjects: true
---

This project serves as the culmination of my academic journey, focusing on automating the ticket generation and registration process through the integration of Artificial Intelligence (AI) and Machine Learning (ML) technologies. It features a user-friendly mobile application for seamless interaction by users and a comprehensive web dashboard tailored for administrative authorities.
