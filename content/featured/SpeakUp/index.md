---
date: '4'
title: 'SPEAK-UP Mobile Application'
cover: 'SPEAKUP.png'
github: 'https://github.com/divyank00/SOS-Car-Accident-Care-System-'
tech:
  - Android
  - Material Design
  - Firebase Auth
  - Firebase Firestore
showInProjects: true
---

Facilitated anonymous posting of doubts, suggestions, and complaints, fostering open communication within the student community. Enabled real-time messaging and discussions among users through Firebase, promoting immediate engagement and response.Empowered a user base of 80 students to voice their concerns and contribute to enhancing the educational environment through constructive feedback and collaboration
