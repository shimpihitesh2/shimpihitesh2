---
date: '3'
title: 'SITRC-COMP'
github: 'https://github.com/shashankpathak7798/SITRC_COMP'
tech:
  - Flutter
  - Firebase

company: ''
showInProjects: true
---

This application was developed specifically for our department and was presented before the NBA committee. The application effectively showcased various facets of our department, including events, academic achievements, sports victories, and upcoming events.
