---
date: '1'
title: 'Bookify'
external: 'https://gitlab.com/shimpihitesh2/flutter-bookify-app'
tech:
  - Flutter
  - Material Design
  - Django
  - PostgreSQL
company: ''
showInProjects: true
---

This application was meticulously crafted with the purpose of facilitating the seamless sharing of college textbooks between senior and junior students within our institution. Its inception stemmed from the recognition of the importance of academic resources and the desire to foster a culture of knowledge exchange and collaboration among students
