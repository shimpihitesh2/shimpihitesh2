---
date: '2'
title: 'Daily.Tech'
external: 'https://github.com/hiteshshimpi-55/Daily.Tech'
tech:
  - Flutter
  - Material Design
  - BLOC
  - Clean Architecture

company: ''
showInProjects: true
---

This application delivers the latest in technology news, employing the BLOC pattern and Clean Architecture for robustness and scalability. Users can seamlessly access curated articles, stay informed, and enjoy an intuitive browsing experience.
